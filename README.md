# Myserver

## Introduction

Ce projet permet de déployer un server Linux. On y trouve des instructions, des fichers de configuration et des scripts.

## Introductions

Clonez le projet dans votre dossier personnel:

```
git clone https://gitlab.com/souleater19/myserver.git
```

Entrez maintenant dans le dossier téléchargé
    cd myserver

Vous pouvez maintenant:
-[installer un server web](#installer_un_server_web_nginx)
-installer un server ssh

## Installer un server web Nginx

#### Quelques éléments sur le réseau

##### Connaitre son adresse ip

Pour connaitre son adresse ip:
```
ip a
```

On cherche la ligne qui contient inet (`ip a |grep inet')

Ceci est l'adresse de notre ordinateur sur le réseau interne.

A ne pas confondre avec votre adresse ip publique,
qui est l'adresse de votre BOX, accessible depuis Internet.

Pour obtenir cette dernière, taper "what is my ip ?" dans la barre de recherche de Google.

##### Connaitre ses noms d'hote

On utilise le fichier `/etc/hosts`
qui fonctionne comme un serveur DNS: il s'agit d'un tableau de correspondance
entre des adresses IP et des noms d'hote.

On va ainsi pouvoir communiquer avec l'ordinateur un utilisant au choix
l'adresse IP ou le nom d'hote

Ex:
```
127.0.0.1       localhost
127.0.1.1       resonance
```

##### Installer nginx

```
sudo apt install nginx
```

Pour vérifier que le server fonctionne, on utilise:
```
sudo systemctl status nginx
```

Si le status de nginx est "Inactive", on le démarre avec

sudo systemctl start nginx
```

##### Modifier le contenu

La page par défaut d'acces à nginx se trouve à ` /var/www/html/index.nginx-debian.html`

Modifier ce fichier pour changer la page d'accueil.
